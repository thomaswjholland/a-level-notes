                                   YOU
                         What is this?

                                   ME
                         This is my place for storing all of
                         my A Level notes that I take.
                         Before, it was all close off to the
                         public, but now I have it open
                         source and available to anybody!

                                   YOU
                         So can this replace me taking
                         notes? No more revision guides?

                                   ME
                         Sadly, this will never be able to
                         replace you taking notes for
                         several reasons, including that I
                         am only human, so there will be
                         error and this will never be as in
                         depth or as comprehensive as a
                         textbook or revision guide.

                                   YOU
                         Errors?

                                   ME
                         Unfortunately, yes. I hope that
                         these are only spelling errors and
                         that they don't stop people from
                         understanding the content.

                                   YOU
                         Why are you only doing Biology,
                         Chemistry, Physics and Maths?

                                   ME
                         This is because these are the
                         subjects that I am talking for A
                         Level.

                                   YOU
                         So is it free?

                                   ME
                         Yes, I thought about selling them
                         at some point, but I didn't really
                         have the time or the effort to
                         bother

                                   ENTHUSIASTIC YOU
                         So is there anything I can do to
                         help?

                                   ME
                         Well, as time goes on, I have less
                         of it, and so if you would like to
                         go through it and rotate any of the
                         documents (my printer can be very
                         special).

                                   YOU
                         What is the "buy me a coffee"
                         button below?

                                   ME
                         Students run on coffee, and it
                         would be great if you could get me
                         one. There is no need but thank you
                         if you do.

                                   YOU
                         I have some more questions!

                                   ME
                         If there is an issue with my work
                         please submit them under the
                         "Issues" area here on bitbucket. If
                         you have a question, you can find
                         me on reddit: u/thomaswholland.

[![Buy Me A Coffee](https://cdn.buymeacoffee.com/buttons/default-white.png)](https://www.buymeacoffee.com/thomaswjholland)
